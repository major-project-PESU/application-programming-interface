// var authController = require('../controllers/auth'),
var express = require('express'),
passportService = require('../../config/auth');
passport = require('passport');

var requireAuth = passport.authenticate('jwt',{session: false});
var requireLogin = passport.authenticate('local', {session:false});
var requirePharmacistLogin = passport.authenticate('pharmacist-local-login',{session:false});
var requirePharmacistAuth = passport.authenticate('jwt-pharmacist',{session: false});

var patientController = require('../controllers/patient')

module.exports = (app) => {
app.post('/patient/address/add',requireAuth,patientController.addAddress);
// app.post('/patient/address/edit',patientController.editAddress);
app.post('/patient/address/delete',requireAuth,patientController.deleteAddress);
app.get('/patient/address',requireAuth,patientController.getAddress)
}
