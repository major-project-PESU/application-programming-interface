var authController = require('../controllers/auth'),
express = require('express'),
passportService = require('../../config/auth');
passport = require('passport');

var requireAuth = passport.authenticate('jwt',{session: false});
var requireLogin = passport.authenticate('local', {session:false});
var UPLOAD_PATH = '/home/rohanmallya/Desktop/Rohan/Sem8/Project/ServerFiles/Temp'

var multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, UPLOAD_PATH)
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
})
var upload = multer({storage:storage})


var prescriptionController = require('../controllers/prescription');

module.exports = (app) => {
  app.post('/prescription/save', requireAuth, upload.single('image'), prescriptionController.save);

  app.get('/prescription/get/', requireAuth, prescriptionController.get);

  app.get('/prescription/get/:id/:prescID', prescriptionController.getById)
}
