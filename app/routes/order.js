var authController = require('../controllers/auth'),
express = require('express'),
passportService = require('../../config/auth');
passport = require('passport');

var requireAuth = passport.authenticate('jwt',{session: false});
var requireLogin = passport.authenticate('local', {session:false});
var requirePharmacistLogin = passport.authenticate('pharmacist-local-login',{session:false});
var requirePharmacistAuth = passport.authenticate('jwt-pharmacist',{session: false});

var orderController = require('../controllers/order.js')

module.exports = (app) => {
  app.post('/patient/orders/active', requireAuth,orderController.patientGetActiveOrders);
  app.post('/patient/orders/past',requireAuth,orderController.patientGetPastOrders);
  app.post('/patient/orders/all',requireAuth,orderController.patientGetAllOrders)

  app.post("/pharmacist/orders/all",requirePharmacistAuth,orderController.pharmacistGetAllOrders);
  app.post("/pharmacist/order/changestatus",requirePharmacistAuth,orderController.pharmacistChangeStatus)

}
