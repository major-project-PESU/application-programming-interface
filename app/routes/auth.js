var authController = require('../controllers/auth'),
express = require('express'),
passportService = require('../../config/auth');
passport = require('passport');

var requireAuth = passport.authenticate('jwt',{session: false});
var requireLogin = passport.authenticate('local', {session:false});
var requirePharmacistLogin = passport.authenticate('pharmacist-local-login',{session:false});
var requirePharmacistAuth = passport.authenticate('jwt-pharmacist',{session: false});

module.exports = (app) => {
app.post('/patient/register',authController.register);
app.post('/patient/login',requireLogin,authController.login);
app.get('/protec', requireAuth, function(req,res){
   res.send({content: "SUCCESS"});
});

app.get('/protecPharmacist', requirePharmacistAuth, function(req,res){
res.send({content: "SUCCESS"});
});

app.post('/pharmacist/register', authController.pharmacistRegister);
app.post('/pharmacist/login',requirePharmacistLogin, authController.pharmacistLogin);
}
