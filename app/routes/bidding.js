var authController = require('../controllers/auth'),
express = require('express'),
passportService = require('../../config/auth');
passport = require('passport');

var requireAuth = passport.authenticate('jwt',{session: false});
var requireLogin = passport.authenticate('local', {session:false});
var requirePharmacistLogin = passport.authenticate('pharmacist-local-login',{session:false});
var requirePharmacistAuth = passport.authenticate('jwt-pharmacist',{session: false});

var biddingController = require('../controllers/bidding.js')

module.exports = (app) => {
  // app.post('/patient/bids/active',biddingController.patientGetBidsForRequest);

  app.post('/pharmacist/bids/chosen',requirePharmacistAuth,biddingController.pharmacistGetChosenBids);
  app.post('/pharmacist/bids/unseen',requirePharmacistAuth,biddingController.pharmacistsGetUnseenBids);
  app.post('/pharmacist/bids/active',requirePharmacistAuth,biddingController.pharmacistGetActiveBids);

  app.post('/pharmacist/bid/accept',requirePharmacistAuth,biddingController.pharmacistAccept);
  app.post('/pharmacist/bid/image/accept',requirePharmacistAuth,biddingController.pharmacistAcceptImage)
  app.post('/pharmacist/bid/reject',requirePharmacistAuth,biddingController.pharmacistReject);

  app.post('/patient/bids/present',requireAuth,biddingController.patientGetPresentBids);
  app.post('/patient/bid/accept',requireAuth,biddingController.patientAccept)
  // app.post('/patient/bids/unseen',requireAuth,biddingController.patientGetUnseenBids);


}
