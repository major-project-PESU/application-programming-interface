var authController = require('../controllers/auth'),
express = require('express'),
passportService = require('../../config/auth');
passport = require('passport');

var requireAuth = passport.authenticate('jwt',{session: false});
var requireLogin = passport.authenticate('local', {session:false});

var scheduleController = require('../controllers/schedule');

module.exports = (app) => {
  app.post('/schedule/add',requireAuth,scheduleController.addMedicine);
  app.get('/schedule/',requireAuth,scheduleController.getMedicines)
  app.get('/schedule/:yyyy/:mm/:dd',requireAuth,scheduleController.getMedicinesByDate)
  app.post('/schedule/delete',requireAuth,scheduleController.deleteMedicine)
}
