var authController = require('../controllers/auth'),
express = require('express'),
passportService = require('../../config/auth');
passport = require('passport');

var requireAuth = passport.authenticate('jwt',{session: false});
var requireLogin = passport.authenticate('local', {session:false});
var requirePharmacistLogin = passport.authenticate('pharmacist-local-login',{session:false});
var requirePharmacistAuth = passport.authenticate('jwt-pharmacist',{session: false});

var requestController = require('../controllers/request.js')

module.exports = (app) => {
  app.post('/request/checkout',requireAuth,requestController.request);
  app.post('/request/checkout/image',requireAuth,requestController.request)
  // app.post('/request/upload', requestController.byImage);
}
