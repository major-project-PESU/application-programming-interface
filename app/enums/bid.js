exports.bidStatusEnum = {
    REJECT:-1,
    LOST : -2,
    ACCEPT:1,
    UNSEEN:0,
    CHOSEN:2
}
