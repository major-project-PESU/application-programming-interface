var mongoose = require('mongoose');

var medicineJSON = {
  name:String,
  quantity:Number,
  time:String,
  date:String
}


var scheduleJSON = {
    patientID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Patient'
    },
    active: [medicineJSON],
    past: [medicineJSON]
}

var ScheduleSchema = new mongoose.Schema(scheduleJSON);
module.exports = mongoose.model('Schedule', ScheduleSchema);
