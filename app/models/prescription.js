var mongoose = require('mongoose');

var prescriptionJSON = {
  filename: String,
  originalName: String,
  name: String,
  // desc: String,
  patientID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Patient'
  },
  patientName:String,
  created: {type:Date, default:Date.now},
  url:String
}

var PrescriptionSchema = new mongoose.Schema(prescriptionJSON);
module.exports = mongoose.model('Prescription', PrescriptionSchema);
