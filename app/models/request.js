var mongoose = require('mongoose');

var medicineJSON = {

  Name:{
    type:String,
    required:true
  },
  Company:{
    type:String,
  },
  "Generic Name":{
    type:String,
  },
  Type:{
    type:String,
  },
  Dosage:{
    type:String,
  },
  Quantity:{
    type:String,
  },
  Price:{
    type:Number,
    required:true
  },
  AlternativeFor:{
    type:String
  },
  Units:Number,
  Total:Number
}

var addressJSON = {
  name:String,
  fullAddress:String,
  city:String,
  locality:String,
  postalCode:String,
  lat:String,
  lng:String
}


var requestJSON = {
  name: String,
  // desc: String,
  patientID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Patient'
  },
  prescriptionID: {
    type:mongoose.Schema.Types.ObjectId,
    ref: 'Prescription'
  },
  patientName:String,
  created: {type:Date, default:Date.now},
  url:String,
  medicines:[medicineJSON],
  address:addressJSON,
  total:Number,
  date:String,
  time:String,
  status:Number, // -1 done, 0 unattended, 1 attended, 2 served,
  urgent:Boolean,
  by:String
  // status:String
}

var RequestSchema = new mongoose.Schema(requestJSON);
module.exports = mongoose.model('Request', RequestSchema);
