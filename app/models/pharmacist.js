var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var addressJSON = {
  fullAddress:String,
  city:String,
  locality:String,
  postalCode:String,
  lat:String,
  lng:String
}
var pharmacistJSON = {

    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        lowercase: true
    },
    rating: {
        type: Number,
        default:0
    },
    address: addressJSON,
    city:String,
    contactNumber: String,
    bids:[{
      type:mongoose.Schema.Types.ObjectId,
      ref:'Bid'
    }

    ],
    active:[{
      type:mongoose.Schema.Types.ObjectId,
      ref:'Order'}
    ],
    past:[{
      type:mongoose.Schema.Types.ObjectId,
      ref:'Order'}
    ]
    // notifToken: [{
    //     type: String,
    //     unique: true
    // }]

}


var PharmacistSchema = new mongoose.Schema(pharmacistJSON, {
    timestamps: true
});

PharmacistSchema.pre('save', function (next) {

    var user = this;
    var SALT_FACTOR = 5;

    if (!user.isModified('password')) {
        return next();
    }

    bcrypt.genSalt(SALT_FACTOR, function (err, salt) {

        if (err) {
            return next(err);
        }

        bcrypt.hash(user.password, salt, null, function (err, hash) {

            if (err) {
                return next(err);
            }

            user.password = hash;
            next();

        });

    });

});

PharmacistSchema.methods.comparePassword = function (passwordAttempt, cb) {

    bcrypt.compare(passwordAttempt, this.password, function (err, isMatch) {

        if (err) {
            return cb(err);
        } else {
            cb(null, isMatch);
        }
    });

}

module.exports = mongoose.model('Pharmacist', PharmacistSchema);
