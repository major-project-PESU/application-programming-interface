var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var addressJSON = {
  name:String,
  fullAddress:String,
  city:String,
  locality:String,
  postalCode:String,
  lat:String,
  lng:String
}

var patientJSON = {

    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        lowercase: true
    },
    rating: {
        type: Number,
        default:0
    },
    dob: String,
    addresses: [addressJSON],
    contactNumber: String,
    // notifToken: [{
    //     type: String,
    //     unique: true
    // }]

}


var PatientSchema = new mongoose.Schema(patientJSON, {
    timestamps: true
});

PatientSchema.pre('save', function (next) {

    var user = this;
    var SALT_FACTOR = 5;

    if (!user.isModified('password')) {
        return next();
    }

    bcrypt.genSalt(SALT_FACTOR, function (err, salt) {

        if (err) {
            return next(err);
        }

        bcrypt.hash(user.password, salt, null, function (err, hash) {

            if (err) {
                return next(err);
            }

            user.password = hash;
            next();

        });

    });

});

PatientSchema.methods.comparePassword = function (passwordAttempt, cb) {

    bcrypt.compare(passwordAttempt, this.password, function (err, isMatch) {

        if (err) {
            return cb(err);
        } else {
            cb(null, isMatch);
        }
    });

}

module.exports = mongoose.model('Patient', PatientSchema);
