var mongoose = require('mongoose');

var medicineJSON = {

  Name:{
    type:String,
    required:true
  },
  Company:{
    type:String,
  },
  "Generic Name":{
    type:String,
  },
  Type:{
    type:String,
  },
  Dosage:{
    type:String,
  },
  Quantity:{
    type:String,
  },
  Price:{
    type:Number,
    required:true
  },
  AlternativeFor:{
    type:String
  },
  Units:Number,
  Total:Number
}

var bidJSON = {
  pharmacistID:{
    type:mongoose.Schema.Types.ObjectId,
    ref:"Pharmacist"
  },
  requestID:{
    type:mongoose.Schema.Types.ObjectId,
    ref:"Request"
  },
  patientID:{
    type:mongoose.Schema.Types.ObjectId,
    ref:"Patient"
  },
  status:Number, // -1 if reject, 1 if accept, 0 if unseen
  pharmacyName:String,
  pharmacyAddress:String,
  url:String,
  patientName:String,
  total:Number,
  dispatchDate:String,
  dispatchTime:String,
  delivery:Boolean,
  date:String,
  time:String,
  total:Number,
  medicines:[medicineJSON],
  urgent:Boolean,
  by:String
}

var BidSchema = new mongoose.Schema(bidJSON);
module.exports = mongoose.model('Bid', BidSchema);
