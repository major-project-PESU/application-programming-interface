var mongoose = require('mongoose');
var medicineJSON = {

  Name:{
    type:String,
    required:true
  },
  Company:{
    type:String,
  },
  "Generic Name":{
    type:String,
  },
  Type:{
    type:String,
  },
  Dosage:{
    type:String,
  },
  Quantity:{
    type:String,
  },
  Price:{
    type:Number,
    required:true
  },
  AlternativeFor:{
    type:String
  },
  Units:Number,
  Total:Number
}
var orderJSON = {
  name: String,
  patientName:String,
  // desc: String,
  patientID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Patient'
  },
  pharmacistID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pharmacist'
  },
  pharmacyName:String,
  pharmacyAddress:String,
  requestID:{
    type:mongoose.Schema.Types.ObjectId,
    ref: 'Request'
  },
  status:Number,
  statusDesc:[{
    time:String,
    date:String,
    message:String
  }],
  medicines:[medicineJSON],
  total:Number,
  created: {type:Date, default:Date.now}
}

var OrderSchema = new mongoose.Schema(orderJSON);
module.exports = mongoose.model('Order', OrderSchema);
