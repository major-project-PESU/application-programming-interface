var mongoose = require('mongoose');

var medicineJSON = {

  Name:{
    type:String,
    required:true
  },
  Company:{
    type:String,
    required:true
  },
  Generic:{
    type:String,
    required:true
  },
  Type:{
    type:String,
    required:true
  },
  Dosage:{
    type:String,
    required:true
  },
  Quantity:{
    type:String,
    required:true
  },
  Price:{
    type:Number,
    required:true
  }
}


var MedicineSchema = new mongoose.Schema(medicineJSON);
MedicineSchema.index({Name:'text'})

module.exports = mongoose.model('Medicine', MedicineSchema);
