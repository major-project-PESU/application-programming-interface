var Prescription = require('../models/prescription');
var Patient = require('../models/patient')
var UPLOAD_PATH = '/home/rohanmallya/Desktop/Rohan/Sem8/Project/ServerFiles/Temp'
var fs = require('fs')
var path = require('path')

exports.save = async function(req,res,next) {
  let newPrescription = new Prescription();
  var patientID = req.user.id;
  var patient = await Patient.findById(patientID)
  newPrescription.patientID = patientID;
  newPrescription.patientName = patient.name;
  newPrescription.name = req.body.name;
  newPrescription.filename = req.file.filename;
  newPrescription.originalName = req.file.originalname;
  newPrescription.save(err => {
    if(err){
      return res.sendStatus(400);
    }
    var oldPath = UPLOAD_PATH+"/"+newPrescription.filename;
    var newDir = UPLOAD_PATH+"/"+patientID;
    var newPath = newDir+"/"+newPrescription.filename;
    fs.rename(oldPath,newPath,function(err){
      if(err){
        if(err['errno'] == -2){
          fs.mkdirSync(newDir,{recursive:true},function(err){
            if(err)
              console.log(err)
          });
          fs.rename(oldPath,newPath,function(err){
            if(err)
              console.log(err)
            else
              console.log("File Moved")
          })
          }
      }
        else
          console.log("Done")
    })
    res.status(201).send({newPrescription})
  })
}

exports.get = async function(req,res,next){
  var prescriptions = await Prescription.find({patientID:req.user.id});
  for(var i in prescriptions){
    prescriptions[i]['url'] = req.protocol + "://" + req.get('host') + '/prescription/get/' + prescriptions[i].patientID +"/"+ prescriptions[i]._id;
  }
  res.status(200).send(prescriptions);
}

exports.getById = async function(req,res,next) {
  var prescription = await Prescription.findById(req.params.prescID)
  if(!prescription){
    res.sendStatus(400)
  }
  res.setHeader('Content-Type','image/jpeg');
  fs.createReadStream(path.join(UPLOAD_PATH,req.params.id,prescription.filename)).pipe(res)
}
