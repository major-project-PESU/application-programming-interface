var jwt = require('jsonwebtoken');
var Medicine = require('../models/medicine');

exports.search = async function(req,res,next){
  var med = req.body.name;
  var result = await Medicine.find({$text: {$search:med}});
  return res.status(201).send(result)
}
