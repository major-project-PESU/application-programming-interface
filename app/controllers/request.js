var jwt = require('jsonwebtoken');
var Patient = require('../models/patient');
var Pharmacist = require('../models/pharmacist');
var Request = require('../models/request');
var Bid = require('../models/bid')
var moment = require("moment");
var authConfig = require('../../config/auth');
var requestEnum = require('../enums/request').requestStatusEnum;
var bidEnum = require('../enums/bid').bidStatusEnum;

exports.patientGetActiveRequests = async function(req,res,next){
  var patientID = req.body.id;
  var requests = await Request.find({patientID:patientID}).or([{"status":requestStatus.ACCEPTED},{"status":requestStatus.UNATTENDED}]);
  return res.status(201).send(requests);
}

exports.patientGetPastRequests = async function(req,res,next){
  var patientID = req.body.id;
  var requests = await Request.find({patientID:patientID, status:requestStatus.SERVED});
  return res.status(201).send(requests);
}

exports.requestByCheckout = async function(req,res,next){
  var patientID = req.user.id;
  var urgent = req.body.urgent;
  var by = req.body.by;
  var patient = await Patient.findById(patientID);
  var date = moment();
  var today = date.format('DD/MM/YYYY')
  var time = date.format('hh:mm a')
  var requestJSON = {
    patientID : patientID,
    patientName:patient['name'],
    name:req.body.name,
    medicines : req.body.medicines,
    address: req.body.address,
    status: requestEnum.UNATTENDED,
    total:req.body.total,
    time:time,
    date:today,
    urgent:urgent,
    by:by,
  };
  var request = new Request(requestJSON);
  await request.save();
  var city = requestJSON['address']['city']
  var availablePharmacists = await Pharmacist.find({"city":city});
  if(availablePharmacists.length <= 0){
    return res.status(400).send({"message":"No Pharmacists found nearby"})
  }

  for(var i in availablePharmacists){
    var bidJSON = {
      pharmacistID:availablePharmacists[i]['_id'],
      requestID:request._id,
      patientID:patientID,
      patientName:patient['name'],
      medicines:req.body.medicines,
      status:bidEnum.UNSEEN, // -1 if reject, 1 if accept, 0 if unseen
      pharmacyName:availablePharmacists[i]['name'],
      pharmacyAddress:availablePharmacists[i]['address']['fullAddress'],
      date:today,
      time:time,
      total:request.total,
      urgent:urgent,
      by:by,
    }
    var tempBid = new Bid(bidJSON);
    // availablePharmacists[i]['bids'].push(tempBid["_id"])
    try{
      await tempBid.save();
    }catch(e){
      console.log("Bid Not saved: "+e)
    }
    await availablePharmacists[i].save();

  }
  await request.save();
  return res.status(201).send({"message":"Broadcasted!"})
}

exports.requestByPicture = async function(req,res,next){
  // var prescriptionID = req.params.
  var patientID = req.user.id;
  var prescriptionID = req.body.prescriptionID;
  var url = req.body.url;
  var name = req.body.name;
  var patientName = req.body.patientName;
  var address = req.body.address;
  var urgent = req.body.urgent;
  var by = req.body.by;
  var date = moment();
  var today = date.format('DD/MM/YYYY')
  var time = date.format('hh:mm a')
  var requestJSON = {
    patientID: patientID,
    prescriptionID:prescriptionID,
    url:url,
    name:name,
    patientName : patientName,
    address:address,
    status: requestEnum.UNATTENDED,
    date:today,
    time:time,
    urgent:urgent,
    by:by
  };
  var request = new Request(requestJSON);
  await request.save();
  var city = requestJSON['address']['city']
  var availablePharmacists = await Pharmacist.find({"city":city});
  if(availablePharmacists.length <= 0){
    return res.status(400).send({"message":"No Pharmacists found nearby"})
  }

  for(var i in availablePharmacists){
    var bidJSON = {
      pharmacistID:availablePharmacists[i]['_id'],
      requestID:request._id,
      patientID:patientID,
      url:url,
      patientName:patientName,
      status:bidEnum.UNSEEN, // -1 if reject, 1 if accept, 0 if unseen
      pharmacyName:availablePharmacists[i]['name'],
      pharmacyAddress:availablePharmacists[i]['address']['fullAddress'],
      date:today,
      time:time,
      urgent:urgent,
      by:by
    }
    var tempBid = new Bid(bidJSON);
    // console.log(temBid)
    // availablePharmacists[i]['bids'].push(tempBid["_id"])
    try{
      await tempBid.save();
    }catch(e){
      console.log("Bid Not saved: "+e)
    }
    await availablePharmacists[i].save();

  }
  await request.save();
  return res.status(201).send({"message":"Broadcasted!"})
}


exports.request = async function(req,res,next){
  var patientID = req.user.id;
  var patient = await Patient.findById(patientID);

  var prescriptionID = req.body.prescriptionID;
  var url = req.body.url;
  var medicines = req.body.medicines;
  var name = req.body.name;
  var patientName = patient['name'];
  var address = req.body.address;
  var urgent = req.body.urgent;
  var by = req.body.by;
  var date = moment();
  var total = req.body.total;
  var today = date.format('DD/MM/YYYY')
  var time = date.format('hh:mm a')

  var requestJSON = {
    patientID : patientID,
    patientName:patientName,
    name:name,
    address: address,
    status: requestEnum.UNATTENDED,
    total:total,
    time:time,
    date:today,
    urgent:urgent,
    by:by,
  };
  if(medicines){
    requestJSON.medicines = medicines;
    requestJSON.total = total;
  }else{
    requestJSON.url = url;
    requestJSON.prescriptionID = prescriptionID
  }
  var request = new Request(requestJSON);
  await request.save();

  var city = requestJSON['address']['city']
  var availablePharmacists = await Pharmacist.find({"city":city});
  if(availablePharmacists.length <= 0){
    return res.status(400).send({"message":"No Pharmacists found nearby"})
  }

  for(var i in availablePharmacists){
    var bidJSON = {
      pharmacistID:availablePharmacists[i]['_id'],
      requestID:request._id,
      patientID:patientID,
      url:url,
      medicines:medicines,
      patientName:patientName,
      status:bidEnum.UNSEEN, // -1 if reject, 1 if accept, 0 if unseen
      pharmacyName:availablePharmacists[i]['name'],
      pharmacyAddress:availablePharmacists[i]['address']['fullAddress'],
      date:today,
      time:time,
      total:total,
      urgent:urgent,
      by:by
    }
    var tempBid = new Bid(bidJSON);
    // console.log(temBid)
    // availablePharmacists[i]['bids'].push(tempBid["_id"])
    try{
      await tempBid.save();
    }catch(e){
      console.log("Bid Not saved: "+e)
    }
    await availablePharmacists[i].save();

  }
  await request.save();
  return res.status(201).send({"message":"Broadcasted!"})
  
}