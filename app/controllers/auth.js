var jwt = require('jsonwebtoken');
var Patient = require('../models/patient');
var Pharmacist = require('../models/pharmacist');
var authConfig = require('../../config/auth');

function generateToken(Patient){
    return jwt.sign(Patient, authConfig.secret, {
        expiresIn: 10080
    });
}

function setInfo(request){
    return {
        _id: request._id,
        email: request.email,
    };
}


exports.login = function(req, res, next){

    var patientInfo = setInfo(req.user);
    res.status(200).json({
        token: 'JWT ' + generateToken(patientInfo),
        patient: patientInfo
    });

}

exports.register = function(req, res, next){

    var email = req.body.email;
    var password = req.body.password;
    var name = req.body.name;
    var dob = req.body.dob;
    var ratings = 0;

    var contactNumber = req.body.contactNumber
    // var notifToken = req.body.notifToken;
    if(!email){
        return res.status(422).send({error: 'You must enter an email address'});
    }

    if(!password){
        return res.status(422).send({error: 'You must enter a password'});
    }

    Patient.findOne({email: email}, function(err, existingPatient){

        if(err){
            return next(err);
        }

        if(existingPatient){
            return res.status(422).send({error: 'That email address is already in use'});
        }

        var patient = new Patient({
            email: email,
            password: password,
            name:name,
            dob:dob,
            contactNumber:contactNumber,
            ratings:ratings
            // notifToken:notifToken
        });
        patient.save(function(err, patient){

            if(err){
                return next(err);
            }

            var patientInfo = setInfo(patient);

            res.status(201).json({
                token: 'JWT ' + generateToken(patientInfo),
                patient: patientInfo
            })

        });

    });
}


// Pharmacist Authentication Routes

exports.pharmacistRegister = function(req, res, next){

    var email = req.body.email;
    var password = req.body.password;
    var name = req.body.name;
    var rating = 0;
    var address = req.body.address;

    var city = address['city'];
    var contactNumber = req.body.contactNumber;
    // var notifToken = req.body.notifToken;

    if(!email){
        return res.status(422).send({error: 'You must enter an email address'});
    }

    if(!password){
        return res.status(422).send({error: 'You must enter a password'});
    }

    Pharmacist.findOne({email: email}, function(err, existingPatient){

        if(err){
            return next(err);
        }

        if(existingPatient){
            return res.status(422).send({error: 'That email address is already in use'});
        }

        var pharmacist = new Pharmacist({
            email: email,
            password: password,
            name: name,
            rating:rating,
            address:address,
            city:city,
            contactNumber: contactNumber,

        });

        pharmacist.save(function(err, pharmacist){

            if(err){
                return next(err);
            }

            var pharmacistInfo = setInfo(pharmacist);

            res.status(201).json({
                token: 'JWT ' + generateToken(pharmacistInfo),
                pharmacist: pharmacistInfo
            })

        });

    });
}



exports.pharmacistLogin = function(req, res, next){

    var pharmacistInfo = setInfo(req.user);

    res.status(200).json({
        token: 'JWT ' + generateToken(pharmacistInfo),
        pharmacist: pharmacistInfo
    });

}
