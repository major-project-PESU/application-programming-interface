var jwt = require('jsonwebtoken');
var Patient = require('../models/patient');
var Pharmacist = require('../models/pharmacist');
var Request = require('../models/request');
var Bid = require('../models/bid');
var Order = require('../models/order')
var moment = require('moment');
var authConfig = require('../../config/auth');
var requestStatus = require('../enums/request').requestStatusEnum
var orderStatus = require('../enums/order').orderStatusEnum;
var bidStatus = require('../enums/bid').bidStatusEnum;

exports.pharmacistGetChosenBids = async function(req,res,next){
  var pharmacistID = req.user.id;
  var bids = await Bid.find({pharmacistID:pharmacistID,status:bidStatus.CHOOSEN});
  return res.status(201).send(bids);
}

exports.pharmacistsGetUnseenBids = async function(req,res,next){
  var pharmacistID = req.user.id;
  var bids = await Bid.find({pharmacistID:pharmacistID,status:bidStatus.UNSEEN});
  return res.status(201).send(bids);

}

exports.pharmacistGetActiveBids = async function(req,res,next){
  var pharmacistID = req.user.id;
  var bids = await Bid.find({pharmacistID:pharmacistID,status:bidStatus.ACCEPT});
  return res.status(201).send(bids);

}

exports.pharmacistAccept = async function(req,res,next){
  var bidID = req.body.bidID;
  var bid = await Bid.findById(bidID);
  var request = await Request.findById(bid.requestID);

  if(req.body.total){
    bid.total = req.body.total;
  }
  bid.dispatchDate=req.body.dispatchDate;
  bid.dispatchTime=req.body.dispatchTime;
  bid.delivery=req.body.delivery;
  // if(req.body.by){
  //   if(moment(req.body.by).isAfter(moment()) )
  // }
  if(bid.status == bidStatus.ACCEPT){
    return res.status(422).send({"error":"Bid has already been accepted"})
  }
  if(request.status == requestStatus.SERVED){
    return res.status(422).send({"error":"Request has been served by another vendor"})
  }
  bid.status = bidStatus.ACCEPT;
  request.status = requestStatus.ACCEPTED;
  await bid.save();
  await request.save();
  return res.status(201).send({"message":"Bid Accepted"});
}


exports.pharmacistReject = async function(req,res,next){
  var bidID = req.body.bidID;
  var bid = await Bid.findById(bidID);
  bid.status = bidStatus.REJECT;
  await bid.save();
}

exports.pharmacistAcceptImage = async function(req,res,next){
  var bidID = req.body.bidID;
  var medicines = req.body.medicines;
  var total = req.body.total;
  var bid = await Bid.findById(bidID);
  var request = await Request.findById(bid.requestID);
  if(bid.status == bidStatus.ACCEPT){
    return res.status(422).send({"error":"Bid has already been accepted"})
  }
  if(request.status == requestStatus.SERVED){
    return res.status(422).send({"error":"Request has been served by another vendor"})
  }
  if(medicines.length == 0){
    return res.status(422).send({"error":"Empty Medicines Array"})
  }
  bid.medicines = medicines;
  bid.total = total
  bid.dispatchDate=req.body.dispatchDate;
  bid.dispatchTime=req.body.dispatchTime;
  bid.delivery=req.body.delivery;
  bid.status = bidStatus.ACCEPT;
  request.status = requestStatus.ACCEPTED;
  await bid.save();
  await request.save();
  return res.status(201).send({"message":"Image Prescription Accepted"})
}

exports.patientGetBidsForRequest = async function(req,res,next){
  var patientID = req.user.id;
  var requestID = req.body.requestID;
  // var request = Request.findById(requestID);
  var bids = await Bid.find({requestID:requestID,status:bidStatus.ACCEPT});
  return res.status(201).send(bids);
}

exports.patientGetPresentBids = async function(req,res,next){
  var patientID = req.user.id;
  var requests = await Request.find({patientID:patientID}).or([{status:requestStatus.ACCEPTED},{status:requestStatus.UNATTENDED}]);
  // requests = requests.toObject();
  if(requests.length == 0){
    return res.status(201).send([])
  }
  for(var i in requests){

    var bidsForRequest = await Bid.find({requestID:requests[i]['_id']});

    var bids = []
    for(var j in bidsForRequest){
      if(bidsForRequest[j]['status'] == bidStatus.UNSEEN){
        continue;
      }
      bids.push({
        "_id":bidsForRequest[j]['_id'],
        "pharmacistID":bidsForRequest[j]['pharmacistID'],
        "status":bidsForRequest[j]['status'],
        "pharmacyName":bidsForRequest[j]['pharmacyName'],
        "medicines":bidsForRequest[j]['medicines'],
        "pharmacyAddress":bidsForRequest[j]['pharmacyAddress'],
        "total":bidsForRequest[j]['total'],
        // "dispatchDetails":bidsForRequest[j]['dispatchDetails'],
        "dispatchDate":bidsForRequest[j]['dispatchDate'],
        "dispatchTime":bidsForRequest[j]['dispatchTime'],
        "delivery":bidsForRequest[j]['delivery'],
      })
    }
    requests[i].set("bids",bids,{strict:false});
  }
  return res.status(201).send(requests);
}

exports.patientGetUnseenBids = async function(req,res,next){
  var patientID = req.user.id;
  var requests = await Request.find({patientID:patientID,status:requestStatus.UNATTENDED});
  return res.status(201).send(requests);
}

exports.patientAccept = async function(req,res,next){
  var bidID = req.body.bidID;
  var bid = await Bid.findById(bidID);
  var request = await Request.findById(bid.requestID);
  if(request.status == requestStatus.SERVED){
    return res.status(422).send({"error":"Request Served by another bid"})
  }
  if(bid.status == bidStatus.ACCEPT){

    var date = moment();
    var today = date.format('DD/MM/YYYY')
    var time = date.format('hh:mm a')
    var orderJSON = {
      name:request.name,
      patientName:request.patientName,
      patientID: request.patientID,
      pharmacistID:bid.pharmacistID,
      requestID:request._id,
      statusDesc:[{
        time:time,
        date:today,
        message:"Order Placed"
      }],
      pharmacyName:bid.pharmacyName,
      pharmacyAddress:bid.pharmacyAddress,
      status:orderStatus.ACCEPTED,
      medicines:bid.medicines,
      total:bid['total'],
    }
    var order = new Order(orderJSON);
    bid.status = bidStatus.CHOSEN;
    request.status = requestStatus.SERVED;
    await order.save();
    await bid.save();
    await request.save();

    var otherBids = await Bid.find({requestID:bid.requestID});
    for(var k in otherBids){
      if(otherBids[k]._id.equals(bid._id)){
        continue;
      }
      otherBids[k].status = bidStatus.LOST;
      await otherBids[k].save();
    }
    return res.status(201).send({"message":"Bid converted to order"})
    // return res.status(422).send({"error":"Bid has not been accepted"})
  }
    return res.status(422).send({"error":"Bid hasn't been placed by pharmacist."})
}

// exports.getBidsForOrder
