var jwt = require('jsonwebtoken');
var Patient = require('../models/patient');
var Pharmacist = require('../models/pharmacist');
var Schedule = require('../models/schedule');
var authConfig = require('../../config/auth');
var moment = require('moment')

// CRUD


exports.addMedicine = async function(req, res, next){
  var patientSchedule = await Schedule.findOne({patientID:req.user.id});
  if(!patientSchedule){
    patientSchedule = new Schedule({patientID:req.user.id})
    await patientSchedule.save();
    patientSchedule = await Schedule.findOne({patientID:req.user.id})
  }
  var meds = req.body.medicines;
  var date = new Date();
  var tempDate = new Date();
  tempDate = moment(tempDate)
  // var endDate = "2019-04-21";
  date = moment(date)
  // endDate = moment(endDate);
  for(var i in meds){
    // console.log(meds[i])

    var endDate = meds[i].endDate;
    endDate = moment(endDate)
    if(endDate.isBefore(date)){
      console.log(endDate,date)
      return res.status(422).send({"error":"End Date has passed"})
    }
    var instructions = meds[i].instructions;
    instructions = instructions.split("")
    if(instructions[0] >= 1 && !meds[i].morningTime){
      return res.status(422).send({"error":"Morning Time not given"})
    }
    if(instructions[1] >= 1&& !meds[i].afternoonTime){
      return res.status(422).send({"error":"Afternoon Time Not Given"})

    }
    if(instructions[2] >= 1 && !meds[i].nightTime){
      return res.status(422).send({"error":"Night time not given"})
    }

    // All checks passed

    var days = meds[i].selectedDays;
    while(date.isBefore(endDate)){
      if(days.includes(parseInt(date.format('d')))){
        if(parseInt(instructions[0]) >= 1){
          var json = {
            name:meds[i].name,
            quantity:instructions[0],
            time:meds[i].morningTime,
            date:date.format('YYYY-MM-DD')
          }
          patientSchedule['active'].push(json)
          // var newSchedule = new Schedule(json);
          // console.log(patientSchedule['active']);
          // await newSchedule.save();
        }
        if(parseInt(instructions[1]) >= 1){
          var json = {
            name:meds[i].name,
            quantity:instructions[1],
            time:meds[i].afternoonTime,
            date:date.format('YYYY-MM-DD')
          }
          patientSchedule['active'].push(json)

          // var newSchedule = new Schedule(json);
          // console.log(patientSchedule['active']);
          // await newSchedule.save();
        }
        if(parseInt(instructions[2]) >= 1){
          var json = {
            name:meds[i].name,
            quantity:instructions[2],
            time:meds[i].nightTime,
            date:date.format('YYYY-MM-DD')
          }
          // var newSchedule = new Schedule(json);
          patientSchedule['active'].push(json)

          // console.log(patientSchedule['active']);
          // await newSchedule.save();
        }
      }
      date.add(1,'day');
      // console.log(date);
    }
    date = tempDate;

  }
  await patientSchedule.save()
  return res.status(200).send({"message":"Successful"})

}


exports.updateMedicine = function(req,res,next){

}

exports.deleteMedicine = async function(req,res,next){
  var medicineID = req.body.medicineID;
  var schedule = await Schedule.findOne({patientID:req.user.id});
  // var active = schedule['active'];
  for(var i in schedule['active']){
    if(schedule['active'][i]._id == req.body.medicineID){
      schedule['active'].splice(i,1);
    }
  }
  await schedule.save()
  res.status(200).send({"message":"deleted"})
}

exports.getMedicines = async function(req,res,next){
  var schedule = await Schedule.findOne({patientID:req.user.id});
  if(schedule){
    return res.status(201).send(schedule['active'])
  }
  return res.status(422).send({"message":"No records found"})
}

exports.getMedicinesByDate = async function(req,res,next){
  var date = req.params.yyyy + "-" + req.params.mm + "-" +req.params.dd;
  var schedule = await Schedule.findOne({patientID:req.user.id});
  var active = schedule['active'];
  var toSend = []
  for(var i in active){
    if(active[i].date === date){
      toSend.push(active[i])
    }
  }
  return res.status(200).send(toSend);
}
