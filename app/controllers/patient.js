var jwt = require('jsonwebtoken');
var Patient = require('../models/patient');
var Pharmacist = require('../models/pharmacist');
var authConfig = require('../../config/auth');


exports.addAddress = async function(req,res,next){
  // var patientId = req.user.id;
  var patient = await Patient.findById(req.user.id);
  if(patient){
    patient['addresses'].push(req.body.address);
    await patient.save();
    return res.status(201).send({"message":"Address Saved"})
  }
  else {
    return res.status(400).send({"error":"Patient Not Found by ID"})
  }
}

exports.deleteAddress = async function(req,res,next){
  var addressID = req.body.addressID;
  var patient = await Patient.findById(req.user.id);
  for(var i = 0; i < patient['addresses'].length; i++){
    if(patient['addresses'][i]._id == addressID){
      patient['addresses'].splice(i,1);
      await patient.save();
      return res.status(200).send({"message":"Address Deleted"})
    }
  }
  return res.status(400).send({"message":"Address Not Found"})

}

exports.getAddress = async function(req,res,next){
  var patient = await Patient.findById(req.user.id);
  res.status(200).send(patient['addresses']);
}
