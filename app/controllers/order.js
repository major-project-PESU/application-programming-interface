var jwt = require('jsonwebtoken');
var Patient = require('../models/patient');
var Pharmacist = require('../models/pharmacist');
var Request = require('../models/request');
var Bid = require('../models/bid');
var Order = require('../models/order')
var moment = require('moment');
var authConfig = require('../../config/auth');
var requestStatus = require('../enums/request').requestStatusEnum
var orderStatus = require('../enums/order').orderStatusEnum;
var bidStatus = require('../enums/bid').bidStatusEnum;


exports.patientGetActiveOrders = async function(req,res,next){
  var patientID = req.user.id;
  var orders = await Order.find({patientID:patientID,status:orderStatus.ACCEPTED});
  console.log(orders)
  return res.status(201).send(orders);
}

exports.patientGetPastOrders = async function(req,res,next){
  var patientID = req.user.id;
  var orders = await Order.find({patientID:patientID, status:orderStatus.DELIVERED});
  return res.status(201).send(orders);
}

exports.patientGetAllOrders = async function(req,res,next){
  var patientID = req.user.id;
  var orders = await Order.find({patientID:patientID});
  return res.status(201).send(orders);
}


exports.pharmacistGetAllOrders = async function(req,res,next){
  var pharmacistID = req.user.id;
  var orders = await Order.find({pharmacistID:pharmacistID})
  return res.status(201).send(orders);
}

exports.pharmacistChangeStatus = async function(req,res,next){
  var pharmacistID = req.user.id;
  var orderID = req.body.orderID;
  var message = req.body.message;
  var delivered = req.body.delivered;

  var order = await Order.findById(orderID);
  if(order['status'] == orderStatus.DELIVERED){
    return res.status(422).send({"error":"Can not change status as request has been delivered already"})
  }
  if(req.body.delivered){
    order['status'] = orderStatus.DELIVERED;
  }
  var date = moment();

  var statusJSON = {
    message:message,
    date:date.format('DD/MM/YYYY'),
    time:date.format('hh:mm a')
  }

  order['statusDesc'].push(statusJSON);
  await order.save();
  return res.status(201).send({"message":"Status Updated"})
}
