# Changelog

##v0.0.1 -> v0.0.2

* Added/Changed Schedule Schema, made it date wise instead of medicine-wise
* Changed algorithm to add medicines, now the function takes an end date and the days of the week on which the med must be taken and marks all the relavent dates in the calender

## v0.0.2 -> v0.0.3

* Added : Option to navigate schedule by date
* Added : Ability to delete a medicine from schedule
* Modified : Removed use of Javascript Date function; Replaced with MomentJS

## v0.0.3 -> v0.0.4

* Added : Medicine model
* Imported sample snapshot of 988 medicines into Mongo Atlas database.
* Added : Search Index on Medicines collection to improve text searching

## v0.0.4 -> v0.0.5

* Added : Image support
* Added : Prescription Model
* Added : Images are now stored in Server Filesystem with Folder Name as UID of patient
* Added : Support for multi-part file upload to disk using multer
* Added : Routes to get all prescriptions, single prescription and route to save prescription added
* Modified : Routes to get prescription eliminated Passport.js middleware as a fix.
* Added : Name and URL field to prescription model

## v0.0.5 -> v0.0.6

* Added : Order Schema, Request Schema
* Added : Skeleton for bidding routes and bidding controllers.
* Modified : Pharmacist Schema to accomodate full address via Geocoding, city and locality.
* Modified : Pharmacist Authorization controller to remove unwanted data from being processed.

## v0.0.6 -> v0.0.7

* Added : Patient Routes to get and add addresses
* Added : Patient route for deleting addresses
* Modified : Patient Schema to take new format of address

## v0.0.7 -> v0.0.8

* Added : Request Controller, Bid Model, and Request Routes.
* Added : Route to broadcast prescription to all pharmacists in a city.
* Added : Status String in Request Model to know is a request is serviced or not
* Added : City String (for efficient searching), Active, Past and Bids array in Pharmacist model
* Added : Route to get active bids of a patient
* Modified : Medicine JSON on Request Model to mimic Medicine JSON from front-end

## v0.0.8 -> v0.0.9

* Added : Controller function to get Active Bids
* Added : Controller function to get active reuqests (Requests that have not been denied)
* Added : Controller function for patient to choose a bid
* Added : Controller function for pharmacist to accept a request or reject a request that is sent by means of add-to-cart
* Added : Enums to track status' of request and bid instead of using integers everywhere
* Modified : Request Model, Order model & Bid Model to accomodate name of patient to save querying while retrieveing requests

## v0.0.9 -> v0.0.10

* Added : LOST status in Bidding Enum to signify the orders that a pharmacy lost after / without bidding
* Added : Routes and Controllers for pharmacist to get unseen request, active requests and Active Request
* Modified : Request Schema to add total amount of request (when medicines are searched and added), date and time of creation to shcema
* Modified : Bid Schema to accomodate date, time, total, dispatchStatus and list of medicines for efficient querying
* Modified : Request Controllers now use MomentJS for time functions instead of JavaScript date function in lieu of efficiency, readability and functionality
* Added : Stronger Error Handling in Pharmacist Bidding routes to prohibit multiple biddings on single order and bidding when order is served
* Added : Support for pharmacist to quote total (provide discount) on the bill total deduced by MRP of medicines in the database.
* Modified : Patient Accept Route now converts the accepted bid to an order and changes the remaining bids on the prescription to LOST status to prevent pharmacies from bidding once the order is placed

## v0.0.10 -> v0.0.11

* Added : Order controller to get active/past/all orders for patient based on patientId
* Added : Order Controller to get active/past/all order for pharmacist based on pharmacistID
* Added : Order controller to change status of a particular order, handled error handling to not allow changes to an already delivered order
* Added : Order Enum to track order status'
* Modified : Order Schema to accomodate status number and status description. (Number indicates delivered or not, description has an array of updates about delivery)
* Modified : Medicine Schema in Order to accomodate unit and totals
* Minor changes in schema fields for correctness.

## v0.0.11 -> v0.0.12

* Added : Routes and controllers to handle broadcasting a request made by uploading a image of a prescription
* Added : Routes and controllers to help pharmacist generate a proforma invoice and to bid when an image of a prescription is broadcasted
* Modified : Medicine Schema to lower strictness
* Modified : Added url in bid model, patient name in prescription model in order to make lesser queries to get more details
* Modified : Separated prescription upload controller function from prescription route handling functions

## v0.0.12 -> v1.0.0

* Modified : Combined routes in request.js
* Added : Support for urgent flag and expected delivery time flag for a request (from customer end)
* Added : Support to signify dispatch date, time and delivery availability from pharmacist end
* Minor changes and bug fixes