* While adding medicines, if until date is Friday and medicine has to be taken on Friday also, the last Friday is skipped (+1 to endDate before while loop)
* Delete Route : Add warning when 0 elements are deleted, appropriate error handlings
* Medicine Controller : error handling check
* Image Uploading : Separate Controllers and Routes; Make folders in Server Directory not /Server/Temp, Test GET routes from phone
* Dont take null values for addresses in Patient Schema


## Bidding controllers

* Write route to get past requests
* On clicking on an active requests, number of bids must be seen. So write a route to get the bids pertaining to an active requests
* Take care of one-pharmacy, one-bid.
* Take care of condition where bidding is done after order confirmation, remove bid from pharmacist's UI after confirmation. (Move to Past array of chosen Pharmacy, remove from others active array)
*
